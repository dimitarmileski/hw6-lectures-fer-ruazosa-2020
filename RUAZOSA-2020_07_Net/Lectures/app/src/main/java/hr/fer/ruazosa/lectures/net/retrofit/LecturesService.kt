package hr.fer.tel.ruazosa.lectures.net.retrofit


import hr.fer.tel.ruazosa.lectures.entity.Course
import hr.fer.tel.ruazosa.lectures.entity.Person
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import retrofit.http.*

interface LecturesService {
    @get:GET("/courses")
    val listOfCourses: List<ShortCourse>

    @GET("/courses/{id}")
    fun getCourse(@Path("id") id: Long?): Course

    @GET("/courses/{id}/students")
    fun getCourseStudents(@Path("id") courseId: Long?): List<ShortPerson>

    @POST("/courses/{courseId}/enrollPerson/{personId}")
    fun enrollPersonToCourse(@Path("personId") personId:Long?, @Path("courseId") courseId: Long?):Boolean?

    @GET("/persons/{id}")
    fun getPerson(@Path("id") id: Long?): Person

    @POST("/persons")
    fun postPerson(@Body person: Person?): Boolean?

    @POST("/courses/{courseId}/unenrollPerson/{personId}")
    fun disenrollPersonFromCourse(@Path("personId") personId: Long?, @Path("courseId") courseId: Long?): Boolean?

    @DELETE("/persons/{id}")
    fun deletePerson(@Path("id") id: Long?): Boolean?

    @GET("/persons")
    fun getPersons(): List<Person>

    @get:GET("/persons")
    val listOfPersons: List<ShortPerson>

}
