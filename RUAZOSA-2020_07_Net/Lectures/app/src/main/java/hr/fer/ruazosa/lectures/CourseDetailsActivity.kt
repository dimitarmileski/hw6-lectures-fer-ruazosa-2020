package hr.fer.ruazosa.lectures

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.*
import hr.fer.tel.ruazosa.lectures.entity.Course
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestFactory
import kotlinx.android.synthetic.main.activity_course_details.*
import kotlinx.android.synthetic.main.activity_main.*

class CourseDetailsActivity : AppCompatActivity() {

    private var course: Course? = null
    private var courseName: TextView? = null
    private var courseDescription: TextView? = null
    private var teacher: TextView? = null
    private var courseStudentsButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course_details)

        courseName = findViewById(R.id.courseName) as TextView
        courseDescription = findViewById(R.id.courseDescription) as TextView
        teacher = findViewById(R.id.courseTeacher) as TextView
        courseStudentsButton = findViewById(R.id.courseStudentsButton) as Button


        studentListView.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->
            val itemAtPosition = parent.getItemAtPosition(position)
            val shortPerson = itemAtPosition as ShortPerson
            val intent = Intent(this@CourseDetailsActivity, StudentDetailsActivity::class.java)
            intent.putExtra("person", shortPerson)
            intent.putExtra("courseId", course?.id)
            startActivity(intent)
        }


        val shortCourse = intent.getSerializableExtra("course") as ShortCourse

        LoadShortCourseTask().execute(shortCourse)


        courseStudentsButton?.setOnClickListener {
            LoadStudentsTask().execute()
        }

        addStudentToCourseBtn?.setOnClickListener {
            val id = studentIdToAddEditText.text.toString().toLong()
            studentIdToAddEditText.setText("")
            AddStudentToCourse(id).execute()
        }

    }

    private inner class LoadShortCourseTask: AsyncTask<ShortCourse, Void, Course?>() {

        override fun doInBackground(vararg sCourse: ShortCourse): Course? {
            val rest = RestFactory.instance
            return rest.getCourse(sCourse[0].id)
        }

        override fun onPostExecute(newCourse: Course?) {
            course = newCourse
            courseName?.text = course?.name
            courseDescription?.text = course?.description

            this@CourseDetailsActivity.teacher?.text = course?.teacher?.name
        }
    }


    private inner  class LoadStudentsTask: AsyncTask<Void, Void, List<ShortPerson>?>() {
        override fun doInBackground(vararg p0: Void?): List<ShortPerson>? {
            val rest = RestFactory.instance
            return rest.getCourseStudents(courseId = course?.id)
        }

        override fun onPostExecute(result: List<ShortPerson>?) {
            updateStudentsList(result)
        }
    }

    private fun updateStudentsList(students: List<ShortPerson>?) {
        if(students != null) {
            val adapter = StudentAdapter(this,
                android.R.layout.simple_list_item_1, students)
            studentListView.adapter = adapter
        } else {
            Toast.makeText(this, "Courses can not be loaded", Toast.LENGTH_SHORT).show()
        }
    }

    private inner class StudentAdapter(context: Context, textViewResourceId: Int, private val shortStudentList: List<ShortPerson>) : ArrayAdapter<ShortPerson>(context, textViewResourceId, shortStudentList)

    @SuppressLint("StaticFieldLeak")
    private inner class AddStudentToCourse(id: Long): AsyncTask<Void, Void, Boolean?>() {

        val id: Long? = id

        override fun doInBackground(vararg params: Void?): Boolean? {
            val rest = RestFactory.instance

            val listOfStudents = rest.getCourseStudents(courseId = course?.id)
            val students = rest.getPersons()

            var flag = false

            for (student in students) {
                if (student.id == id) {
                    flag = true
                    break
                }
            }
            if (!flag)
                return false


            if (listOfStudents != null) {
                for (student in listOfStudents) {
                    if (student.id == id) {
                        return false
                    }
                }
            }
            if (course?.teacher?.id == id)
                return false

            rest.enrollPersonToCourse(id, courseId = course?.id)
            return true
        }

        override fun onPostExecute(result: Boolean?) {
            if (result!!)
                courseStudentsButton?.performClick()
            else
                Toast.makeText(applicationContext, "Student can not be added to this course.", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onResume() {
        super.onResume()
        if (course != null)
            courseStudentsButton?.performClick()
    }
    }
