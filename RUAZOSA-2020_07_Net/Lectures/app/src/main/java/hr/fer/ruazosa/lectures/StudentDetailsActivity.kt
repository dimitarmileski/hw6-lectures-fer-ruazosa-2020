package hr.fer.ruazosa.lectures

import android.annotation.SuppressLint
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import hr.fer.tel.ruazosa.lectures.entity.Person
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestFactory
import hr.fer.ruazosa.lectures.R
import kotlinx.android.synthetic.main.activity_student_details.*


class StudentDetailsActivity : AppCompatActivity() {

    var courseId: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student_details)

        val shortPerson = intent.getSerializableExtra("person") as ShortPerson
        courseId = intent.getLongExtra("courseId", -1)

        LoadShortPersonTask().execute(shortPerson)

        removeStudentFromCourseBtn.setOnClickListener {
            removeStudentFromCourseBtn.isEnabled = false
            RemovePersonFromCourse(shortPerson.id).execute()
            finish()
        }
    }

    @SuppressLint("StaticFieldLeak")
    private inner class LoadShortPersonTask: AsyncTask<ShortPerson, Void, Person?>() {

        override fun doInBackground(vararg sPerson: ShortPerson): Person? {
            val rest = RestFactory.instance
            return rest.getPerson(sPerson[0].id)
        }

        @SuppressLint("SetTextI18n")
        override fun onPostExecute(newPerson: Person?) {
            firstNameTextView.text = newPerson?.firstName
            lastNameTextView.text =  newPerson?.lastName
            roomTextView.text = newPerson?.room
            numberTextView.text = newPerson?.phone
        }
    }

    @SuppressLint("StaticFieldLeak")
    private inner class RemovePersonFromCourse(id: Long?): AsyncTask<Void, Void, Boolean>() {

        val id = id

        override fun doInBackground(vararg params: Void?): Boolean {

            if (courseId == -1L) return false

            val rest = RestFactory.instance
            rest.disenrollPersonFromCourse(id, courseId)
            return true
        }

    }
}
