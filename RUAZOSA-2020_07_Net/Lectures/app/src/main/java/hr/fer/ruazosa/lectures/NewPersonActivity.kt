package hr.fer.ruazosa.lectures

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import hr.fer.tel.ruazosa.lectures.entity.Person
import hr.fer.tel.ruazosa.lectures.net.RestFactory
import kotlinx.android.synthetic.main.activity_new_person.*

class NewPersonActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_person)

        addPersonBtn.setOnClickListener {
            val firstName = personFirstNameEditText.text.toString()
            val lastName = personLastNameEditText.text.toString()
            val room = personRoomEditText.text.toString()
            val number = personNumberEditText.text.toString()
            val person = Person(null, firstName, lastName, room, number)

            NewPersonTask().execute(person)
        }
    }

    private inner class NewPersonTask: AsyncTask<Person?, Void, Boolean?>() {

        override fun doInBackground(vararg params: Person?): Boolean? {
            val rest = RestFactory.instance

            return rest.postPerson(params[0])
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            Toast.makeText(this@NewPersonActivity, "Person successfully created!", Toast.LENGTH_SHORT).show()
            finish()
        }

    }
}
