package hr.fer.ruazosa.lectures

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.*
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestFactory
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private var listView: ListView? = null
    private var loadCourseButton: Button? = null
    private var loadPeopleButton: Button? = null

    private var loadCoursesState: Boolean = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listView = findViewById(R.id.listView) as ListView
        listView?.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->

            if (loadCoursesState) {
                val itemAtPosition = parent.getItemAtPosition(position)
                val shortCourse = itemAtPosition as ShortCourse
                val intent = Intent(this@MainActivity, CourseDetailsActivity::class.java)
                intent.putExtra("course", shortCourse)
                startActivity(intent)
            } else {
                val itemAtPosition = parent.getItemAtPosition(position)
                val shortPerson = itemAtPosition as ShortPerson
            }
        }

        loadCourseButton = findViewById(R.id.loadCourseButton) as Button
        loadCourseButton?.setOnClickListener {
            LoadCoursesTask().execute()
        }

        loadPeopleButton = findViewById(R.id.loadPeopleBtn) as Button
        loadPeopleButton?.setOnClickListener {
            loadCoursesState = false
            LoadPersonsTask().execute()
        }

        deleteStudentBtn.setOnClickListener {
            DeleteStudent(studentId.text.toString().toLong()).execute()
        }

        addPersonFab.setOnClickListener {
            val intent = Intent(this@MainActivity, NewPersonActivity::class.java)
            startActivity(intent)
        }
    }

    private inner class LoadCoursesTask: AsyncTask<Void, Void, List<ShortCourse>?>() {
        override fun doInBackground(vararg params: Void): List<ShortCourse>? {
            val rest = RestFactory.instance

            return rest.getListOfCourses()
        }

        override fun onPostExecute(courses: List<ShortCourse>?) {
            updateCourseList(courses)
        }
    }

    private fun updateCourseList(courses: List<ShortCourse>?) {
        if(courses != null) {
            val adapter = CourseAdapter(this,
                android.R.layout.simple_list_item_1, courses)
            listView?.adapter = adapter
        } else {
            Toast.makeText(this, "Courses can not be loaded!", Toast.LENGTH_SHORT).show()
        }
    }

    private inner class CourseAdapter(context: Context, textViewResourceId: Int, private val shortCourseList: List<ShortCourse>) : ArrayAdapter<ShortCourse>(context, textViewResourceId, shortCourseList)



    private inner class DeleteStudent(id: Long?): AsyncTask<Void, Void, Boolean?>() {
        val id = id
        override fun doInBackground(vararg params: Void?): Boolean? {
            val rest = RestFactory.instance
            val courses = rest.getListOfCourses() ?: return false
            val students = rest.getPersons()
            var flag = false

            for (student in students) {
                if (student.id == id) {
                    flag = true
                    break
                }
            }

            if (!flag)
                return false

            for (course in courses) {
                try {
                    rest.disenrollPersonFromCourse(id, course.id)
                } catch (ex: Exception) {}

            }
            rest.deletePerson(id)
            return true
        }

        override fun onPostExecute(result: Boolean?) {
            if (result!!)
                Toast.makeText(applicationContext, "Student removed!", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(applicationContext, "Could not remove student", Toast.LENGTH_SHORT).show()
        }

    }

    private inner class LoadPersonsTask : AsyncTask<Void, Void, List<ShortPerson>?>() {

        override fun doInBackground(vararg params: Void?): List<ShortPerson>? {
            val rest = RestFactory.instance
            return rest.getListOfPersons()
        }

        override fun onPostExecute(result: List<ShortPerson>?) {
            updatePersonsList(result)
        }

        private fun updatePersonsList(persons: List<ShortPerson>?) {
            if (persons != null) {
                val adapter = PersonsAdapter(this@MainActivity, android.R.layout.simple_list_item_1, persons)
                listView?.adapter = adapter
            } else {
                Toast.makeText(applicationContext, "Could not load people.", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private inner class PersonsAdapter(
        context: Context,
        textViewResourceId: Int,
        private val shortPersonsList: List<ShortPerson>
    ) : ArrayAdapter<ShortPerson>(context, textViewResourceId, shortPersonsList)

}
